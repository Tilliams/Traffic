import pygame
import time
import random

pygame.init()


FPS = 60

RED = (255, 0, 0)
BLUE = (0, 0, 200)
BRIGHT_BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
GRAY = (128, 128, 128)
BRIGHT_PURPLE = (255, 0, 255)
PURPLE = (200, 0, 200)

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

WIDTH, HEIGHT = 800, 500

CAR_WIDTH = 73
CAR_HEIGHT = 96



gameDisplay = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("A bit racey")
carImg = pygame.image.load("Car.png")
enemyImg = pygame.image.load("Enemy.png")
clock = pygame.time.Clock()


def game_score(score):
    score_display("Score : {}".format(score))

def button(msg, x, y, w, h, ic, ac, font_color, action = None):
    mouse = pygame.mouse.get_pos()
    clicked = pygame.mouse.get_pressed()

    if x+w > mouse[0] > x and y+h > mouse[1] > y:
        pygame.draw.rect(gameDisplay, ac,(x,y,w,h))
        if clicked[0] == 1 and action != None:
            action()

        
    else:
        pygame.draw.rect(gameDisplay, ic,(x,y,w,h))

    smallText = pygame.font.Font("freesansbold.ttf",20)
    textSurf, textRect = text_objects(msg, smallText, font_color)
    textRect.center = ( (x+(w/2)), (y+(h/2)) )
    gameDisplay.blit(textSurf, textRect)






def things(X_THING, Y_THING, WIDTH_THING, HEIGHT_THING, COLOR):
    #pygame.draw.rect(gameDisplay, COLOR, [X_THING, Y_THING, WIDTH_THING, HEIGHT_THING])
    gameDisplay.blit(enemyImg, (X_THING, Y_THING))

def show_car(x, y):
    gameDisplay.blit(carImg, (x, y))


def text_display(message, pos = (WIDTH // 2, HEIGHT // 2)):
    messageFont = pygame.font.Font("freesansbold.ttf", 100)
    MessageSurf, MessageRect = text_objects(message, messageFont, WHITE)
    MessageRect.center = pos
    gameDisplay.blit(MessageSurf, MessageRect)


    pygame.display.update()

    time.sleep(2)
    gameLoop()

def score_display(message):
    messageFont = pygame.font.SysFont(None, 40)
    MessageSurf, MessageRect = text_objects(message, messageFont, RED)
    MessageRect.center = (WIDTH  // 2, 20)
    gameDisplay.blit(MessageSurf, MessageRect)


def text_objects(words, style, color):
    wordSurface = style.render(words, True, color)
    return wordSurface, wordSurface.get_rect()    


def collide():
    text_display("YOU CRASHED")


def gameIntro():

    intro = True

    while intro:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()


        gameDisplay.fill(BLACK)
        largeText = pygame.font.Font('freesansbold.ttf', 115)
        TextSurface, TextRectangle = text_objects("Dodge Car", largeText, WHITE)
        TextRectangle.center = ((WIDTH/2), (HEIGHT/2))
        gameDisplay.blit(TextSurface, TextRectangle)

        mouse = pygame.mouse.get_pos()

        #pygame.draw.rect(gameDisplay, BLUE, (150, 350, 150, 75))
        #pygame.draw.rect(gameDisplay, PURPLE, (550, 350, 150, 75))
        
        button("QUIT!", 550, 350, 150, 75, PURPLE, BRIGHT_PURPLE, WHITE, pygame.quit)
        button("GO!", 150, 350, 150, 75, BLUE, BRIGHT_BLUE, WHITE, gameLoop)
        pygame.display.update()
        clock.tick(FPS)
        




def gameLoop():

    LANE_Ys = random.randint(0, HEIGHT)

    SCORE = 0

    X = WIDTH * 0.44
    Y = HEIGHT * 0.8

    X_CHANGE = 0
    Y_CHANGE = 0

    STARTX_THING = random.randrange(0, WIDTH, WIDTH//4)
    STARTY_THING = -600
    SPEED_THING = 7
    THING_WIDTH = 73
    THING_HEIGHT = 94
    

    leaveGame = False
    
    while not leaveGame:
        
        #KEY EVENTS (ARROWS, QUIT)
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
                
        
            if event.type == pygame.KEYDOWN:
                #gameDisplay.fill(GREEN)
                if event.key == pygame.K_LEFT:
                    X_CHANGE = -7

                if event.key == pygame.K_RIGHT:
                    X_CHANGE = 7
            
                if event.key == pygame.K_DOWN:
                    Y_CHANGE = 7
            
                if event.key == pygame.K_UP:
                    Y_CHANGE = -7

                if event.key == pygame.K_q:
                    pygame.quit()
                    quit()

            if event.type == pygame.KEYUP:
                if event.key == pygame.K_RIGHT or event.key == pygame.K_LEFT:
                    X_CHANGE = 0

                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    Y_CHANGE = 0
                    
        #ALTER IMAGES POSITION
        X += X_CHANGE
        Y += Y_CHANGE

        gameDisplay.fill(GRAY)

        #gameDisplay.blit(LANES)




        #DRAW BOXES TO SCREEN AND MOVE 7 PIXELS AT A SECOND
        things(STARTX_THING, STARTY_THING, THING_WIDTH, THING_HEIGHT, WHITE)
        
        STARTY_THING += SPEED_THING

        
        show_car(X, Y)

        #CRASH WITH EDGE OF SCREEN
        if X > WIDTH - CAR_WIDTH or X < 0 or Y < 0 or Y > HEIGHT - CAR_HEIGHT:
            collide()


        game_score(SCORE)

        if STARTY_THING > HEIGHT:
            STARTY_THING = 0 - THING_HEIGHT  #START A NEW FALLING BOX
            STARTX_THING = random.randrange(0, WIDTH - THING_WIDTH) #START AT A RANDOM PLACE ACROSS THE SCREEM

            #INCREMENT THE SCORE AND MAKE THE GAME HARDER (INCREASE THE SPEED OF THE BLOCKS)
            SCORE += 1
            SPEED_THING += 0.5


        #CRASH WITH BLOCKS
        if Y < STARTY_THING + THING_HEIGHT:
            if X > STARTX_THING and X < STARTX_THING + THING_WIDTH or X+CAR_WIDTH > STARTX_THING and X+CAR_WIDTH < STARTX_THING + THING_WIDTH:
                collide()

               
        pygame.display.update()

        clock.tick(FPS)

gameIntro()
#gameLoop()
#pygame.quit()
#quit()
